package routes;

import java.util.Iterator;
import java.text.MessageFormat;
import org.apache.oozie.client.WorkflowAction;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.ReadablePeriod;
import org.apache.oozie.client.WorkflowJob;
import org.joda.time.Duration;
import org.joda.time.format.PeriodFormatterBuilder;
import org.apache.oozie.client.OozieClient;
import utils.PropertiesFile;
import org.apache.camel.Exchange;
import java.util.Properties;
import org.apache.log4j.Logger;
import org.apache.camel.Processor;

public class OozieProcessor implements Processor

{
    private static final Logger LOG;
    private Properties prop;
    private String company;
    private String folder;

    //@Override
    public void process(final Exchange exchange) {

        this.prop = PropertiesFile.getInstance().getProperties();
        this.checkProperties();
        this.folder = (String)exchange.getIn().getHeader("dateFolder");
        this.company = (String)exchange.getIn().getHeader("company");
        
        if (this.folder == null || this.company == null) {
            OozieProcessor.LOG.error("Folder or Company not defined in route for oozie processor. folder:" + this.folder + " company:" + this.company);
            System.exit(1);
        }

        final OozieClient wc = new OozieClient(this.prop.getProperty("oozieLocation", ""));
        OozieProcessor.LOG.info("oozie Location is " + wc.getOozieUrl());

        if ("".equals(wc.getOozieUrl())) {
            OozieProcessor.LOG.error("OozieLocation must be configured");
            System.exit(1);
        }

        final Properties conf = wc.createConfiguration();
        conf.setProperty("nameNode", this.prop.getProperty("nameNode"));
        conf.setProperty("jobTracker", this.prop.getProperty("jobTracker"));
        conf.setProperty("queueName", this.prop.getProperty("queueName"));
        conf.setProperty("dateFolder", this.folder);
        conf.setProperty("companyFolder", this.company);
        conf.setProperty("oozie.libpath", "${nameNode}/user/oozie/share/lib");
        conf.setProperty("oozie.use.system.libpath", "true");
        conf.setProperty("user.name", this.prop.getProperty("user.name"));
        conf.setProperty("oozie.wf.application.path", this.prop.getProperty("oozieWorkflowFolder"));
        conf.setProperty("oozie.wf.validate.ForkJoin", "false");

        final PeriodFormatter hourAndminutesAndSeconds = 
        		new PeriodFormatterBuilder()
        		.printZeroAlways()
        		.appendHours()
        		.appendSeparator(" hours ")
        		.appendMinutes()
        		.appendSeparator(" minutes and")
        		.appendSeconds()
        		.appendSeparator(" seconds ")
        		.toFormatter();

        try {
            final String jobId = wc.run(conf);
            OozieProcessor.LOG.info("Workflow job, " + jobId + " submitted");
            Duration d = Duration.standardSeconds(0L);

            while (wc.getJobInfo(jobId).getStatus() == WorkflowJob.Status.RUNNING) {
                OozieProcessor.LOG.info(String.format("Workflow job running for %s...", hourAndminutesAndSeconds.print(d.toPeriod())));
                this.printWorkflowInfo(wc.getJobInfo(jobId));
                Thread.sleep(Integer.parseInt(this.prop.getProperty("waitSeconds")) * 1000);
                d = d.plus(Integer.parseInt(this.prop.getProperty("waitSeconds")) * 1000);

            }

            OozieProcessor.LOG.info(String.format("Workflow job completed in %s ...", hourAndminutesAndSeconds.print(d.toPeriod())));
            this.printWorkflowInfo(wc.getJobInfo(jobId));

        }

        catch (Exception r) {
            OozieProcessor.LOG.error("Errors while waiting for Oozie job to finish. System will exit", r);
            System.exit(1);
        }
    }

    private void printWorkflowInfo(final WorkflowJob wf) {

        OozieProcessor.LOG.info("Application Path   : " + wf.getAppPath());
        OozieProcessor.LOG.info(" Name   : " + wf.getAppName());
        OozieProcessor.LOG.info(" Status : " + wf.getStatus());
        OozieProcessor.LOG.info(" Actions:");

        for (final WorkflowAction action : wf.getActions()) {
            OozieProcessor.LOG.info(MessageFormat.format("   Name: {0} Type: {1} Status: {2}", action.getName(), action.getType(), action.getStatus()));
        }
    }

    public void checkProperties() {

        if ("".equalsIgnoreCase(this.prop.getProperty("waitSeconds")) || "".equalsIgnoreCase(this.prop.getProperty("nameNode")) || "".equalsIgnoreCase(this.prop.getProperty("oozieLocation")) || "".equalsIgnoreCase(this.prop.getProperty("jobTracker")) || "".equalsIgnoreCase(this.prop.getProperty("queueName")) || "".equalsIgnoreCase(this.prop.getProperty("companyFolder")) || "".equalsIgnoreCase(this.prop.getProperty("user.name")) || "".equalsIgnoreCase(this.prop.getProperty("oozieWorkflowFolder"))) {
            OozieProcessor.LOG.error("Properties not fully configured: nameNode:" + this.prop.getProperty("nameNode") + " oozieLocation:" + this.prop.getProperty("oozieLocation") + " jobTracker:" + this.prop.getProperty("jobTracker") + " queueName:" + this.prop.getProperty("queueName") + " companyFolder:" + this.prop.getProperty("companyFolder") + "user.name:" + this.prop.getProperty("user.name") + " waitSeconds:" + this.prop.getProperty("waitSeconds") + " oozieWorkflowFolder:" + this.prop.getProperty("oozieWorkflowFolder"));
            System.exit(1);
        }

        else {
            OozieProcessor.LOG.info("Properties are: nameNode:" + this.prop.getProperty("nameNode") + " oozieLocation:" + this.prop.getProperty("oozieLocation") + " jobTracker:" + this.prop.getProperty("jobTracker") + " queueName:" + this.prop.getProperty("queueName") + " companyFolder:" + this.prop.getProperty("companyFolder") + "user.name" + this.prop.getProperty("user.name") + " waitSeconds:" + this.prop.getProperty("waitSeconds") + " oozieWorkflowFolder:" + this.prop.getProperty("oozieWorkflowFolder"));
        }
    }

    static {
        LOG = Logger.getLogger(OozieProcessor.class);
    }

}
