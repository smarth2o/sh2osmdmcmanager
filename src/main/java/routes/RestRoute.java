package routes;

import org.apache.camel.processor.aggregate.AggregationStrategy;
import org.apache.camel.Expression;
import org.apache.camel.model.RouteDefinition;
import org.apache.camel.model.SplitDefinition;
import org.apache.camel.model.ProcessorDefinition;
import org.apache.camel.model.AggregateDefinition;
import org.apache.camel.model.ExpressionNode;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.model.ChoiceDefinition;
import org.apache.camel.Predicate;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.apache.camel.builder.RouteBuilder;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


@Component
public class RestRoute extends RouteBuilder
{
    private static final Logger LOG;
    private String dateFolder;
    private Predicate stopfile;
    private Predicate finishedMeter;
    private int doDBCorectMeterReading() throws SQLException{
	    Connection con = null;
	    String url = "jdbc:mysql://10.10.181.20:3308/smarth2o";
	    String user = "superadmin";
	    String password = "dsmsmarth2o";

	    con = DriverManager.getConnection(url, user, password);

	    CallableStatement cStmt = con.prepareCall("{call adjustMeterReadings()}");
	    return cStmt.executeUpdate();
    }

    public RestRoute() {
        super();
        this.dateFolder = "";
        this.stopfile = this.header("CamelFileName").isEqualTo("stop.txt");
        this.finishedMeter = this.header("finishedMeter").isEqualTo("true");
    }

    @Override
    public void configure() throws Exception {

        this.from("file:{{app.input.directory}}?readLock=changed&readLockCheckInterval=3000")
	        .routeId("mainEntry")
	        .choice()
	        .when()
	        .simple("${file:onlyname} contains '{{app.SESfileprefix}}'  and ${file:ext} == '{{app.SESfiletype}}'")
	        .to("direct:SESProc")
	        .to("file:{{app.sent.directory}}")
	        .endChoice().otherwise()
	        .to("file:{{app.error.directory}}")
	        .endChoice().otherwise()
	        .when(this.stopfile)
	        .to("file:{{app.output.directory}}");


	this.from("direct:SESProc")
		.log("new file ${file:onlyname}")
        	.to("log:com.ses.file?showAll=true&multiline=true")
        //	.split()
        //	.tokenizeXML("register")
	//	.streaming()
	//	.parallelProcessing()
        //	.log("starting aggregation of tokenized register")
        //	.aggregate(this.constant(true), new SESLineAggregator())
	//	.parallelProcessing()
        //	.completionPredicate(this.finishedMeter)
        //	.completionTimeout(10000L)
        //	.log("removing new lines")
        //	.setBody(this.body().append("</registers>"))
        //	.transform()
        //	.groovy("request.body.replace(\"\\r\\n\",\"\")")
        //	.transform()
        //	.groovy("request.body.replace(\"\\n\",\"\")")
        //	.setBody(this.body().append("\n"))
        //	.aggregate(this.constant(true), new SESBodyAggregator())
	//	.parallelProcessing()
        //	.completionTimeout(10000L)
        	.setHeader("CamelFileName", this.simple("{{app.SESfileprefix}}${date:now:hhmm}"))
        	.setHeader("dateFolder", this.simple("${date:now:yyyyMMddhhmm}"))
        	.setHeader("company", this.simple("ses"))
        	.log("writing file " + this.header("dateFolder") + " to  " + this.simple("{{app.output.directory}}"))
        	.to("file:{{app.output.directory}}?fileExist=Override")
        	.log("sending to entry point of hadoop")
        	.recipientList(this.simple("sftp://hdfsftpuser@{{app.HDFSendpoint}}/${header.dateFolder}/{{app.generatedFolder}}?privateKeyFile={{app.privateKey}}"))
        	.log("sending to Oozie for processing")
        	.process(new OozieProcessor())
        	.log("finished processing!")
		.process(new Processor() {
	     //@Override
             public void process(final Exchange exchange) throws Exception {
	     		LOG.info("Starting to correct meter_reading - calling stored procedure.");
			try {
		    		int ret = doDBCorectMeterReading();
				if (ret >=0)
		    		LOG.info("Finished ok with meter_reading stored procedure.");
				else
		    		LOG.error("Failed sored procedure call with code:"+ ret);
		    	}
		    	catch (SQLException e) {
		    	  LOG.error("Failed meter_reading stored procedure."+ e.toString());
		    	}
		    }
		});
	}

    static {
        LOG = Logger.getLogger(RestRoute.class);

    }

}
