package main;

import org.apache.camel.CamelContext;
import org.apache.camel.component.properties.PropertiesComponent;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.log4j.Logger;
import routes.RestRoute;

public class MainContext
{
  private static Logger LOG = Logger.getLogger(MainContext.class);

  public static void main(String[] args) throws InterruptedException {
    new MainContext();
  }

  MainContext() throws InterruptedException
  {
    CamelContext context = new DefaultCamelContext();
    PropertiesComponent pc = new PropertiesComponent();
    pc.setLocation("classpath:camel-ride.properties");
    context.addComponent("properties", pc);
    try {
      context.addRoutes(new RestRoute());
    } catch (Exception e) {
      LOG.error("Failed loading routes", e);
    }

    LOG.info("Context loaded - starting...");
    try {
      context.start();
    } catch (Exception e) {
      LOG.error("Failed starting context", e);
    }

    Thread.currentThread().join();
  }
}