package utils;

import java.io.InputStream;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import org.apache.commons.io.IOUtils;
import java.util.Properties;
import org.apache.log4j.Logger;

public class PropertiesFile
{
    private static final Logger LOG;
    public static final String TAG = "PropertiesFile";
    private static PropertiesFile propertiesFile;
    private Properties properties;

    private PropertiesFile() {
        super();
        final InputStream inputStream = this.getClass().getResourceAsStream("/application.properties");
        this.properties = new Properties();

        try {
            final String propertyFileContents = IOUtils.toString(inputStream, "UTF-8");
            this.properties.load(new StringReader(propertyFileContents.replace("\\", "\\\\")));
            if (this.properties == null) {
                throw new IOException("Properties is null - is application.properties available ? ");
            }
        }

        catch (IOException e) {
            PropertiesFile.LOG.error("Error on creation of PropertiesFile ", e);
        }
    }
   
    public static PropertiesFile getInstance() {
        if (PropertiesFile.propertiesFile == null) {
            PropertiesFile.propertiesFile = new PropertiesFile();
        }
        return PropertiesFile.propertiesFile;
    }

    public Properties getProperties() {
        return this.properties;
    }

    static {
        LOG = Logger.getLogger(PropertiesFile.class);

    }
}